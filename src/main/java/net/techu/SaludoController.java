package net.techu;

import net.bbva.bibliCalculus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaludoController {

    @RequestMapping("/Saludo")
    public String index() {
        bibliCalculus calculadora = new bibliCalculus();
        Double resultado = calculadora.ObtenerPrecioFinal(100);
        return "Hola, yo soy tu API y el precio final de 100 es " + String.valueOf(resultado);
    }
}
