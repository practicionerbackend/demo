package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
class ProductoV2Controller {

    private ArrayList<Producto> listaProductos = null;
    public ProductoV2Controller() {
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1,"PR1", 27.35));
        listaProductos.add(new Producto(2,"PR2", 18.33));
    }

    //Obtener Productos
    @GetMapping(value="/V2/Productos",produces="application/json")
    public ResponseEntity<List<Producto>> obtenerListado()
    {
        System.out.println("Obtener todos los productos");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    //Obtener Productos por ID
    @GetMapping(value="/V2/Productos/{id}",produces="application/json")
    public ResponseEntity<Producto> obtenerProductoId(@PathVariable int id)
    {
        System.out.println("Obtener productos por ID");
        Producto resultado =null;
        ResponseEntity<Producto> respuesta= null;
        try
        {
            resultado= listaProductos.get(id);
            respuesta= new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            respuesta= new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    //Icluir Producto por codigo
    @PostMapping(value="/V2/Productos")
    public ResponseEntity<String> addProducto() {
        System.out.println("Añado producto por codigo");
        listaProductos.add(new Producto(99,"PR3",100.55));
        return new ResponseEntity<>("Producto Creado Correctamente", HttpStatus.CREATED);
    }

    //Incluir Producto por Postman
    @PostMapping(value="/V2/Productos/{nom}")
    public ResponseEntity<String> addProductoconNombre(@RequestBody Producto newProduct){
        System.out.println("Añado producto por Postman");
        System.out.println(newProduct.getId());
        System.out.println(newProduct.getNombre());
        System.out.println(newProduct.getPrecio());
        //listaProductos.add(new Producto(10,nombre,200.55));
        listaProductos.add(newProduct);
        return new ResponseEntity<>("Producto Creado Correctamente", HttpStatus.CREATED);
    }

    //Modificar Productos por ID
    @PutMapping (value="/V2/Productos/{id}")
    public ResponseEntity <String> ModificarProductoId(@PathVariable int id, @RequestBody Producto cambios){
        ResponseEntity <String> respuesta= null;
        System.out.println("Modificar productos por ID");
        try
        {
            Producto productoAModificar = listaProductos.get(id);
            System.out.println("Voy a modificar el producto");
            System.out.println("Precio actual: " + String.valueOf(productoAModificar.getPrecio()));
            System.out.println("Precio nuevo: " + String.valueOf(cambios.getPrecio()));
            productoAModificar.setNombre(cambios.getNombre());
            productoAModificar.setPrecio(cambios.getPrecio());
            listaProductos.set(id, productoAModificar);
            respuesta = new ResponseEntity<>(HttpStatus.ACCEPTED);

        }
        catch (Exception ex)
        {
            respuesta= new ResponseEntity<>("No se ah encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    //Modificar todos los Productos
    @PutMapping("/V2/Productos")
    public ResponseEntity<String> subirPrecio()
    {
        System.out.println("Modificar todos los productos");
        ResponseEntity<String> resultado = null;
        for (Producto p:listaProductos) {
            p.setPrecio(p.getPrecio()*1.25);
        }
        resultado = new ResponseEntity<>(HttpStatus.ACCEPTED);
        return resultado;
    }

    //Eliminar todos los Productos
    @DeleteMapping(value="/V2/Productos/")
    public ResponseEntity <List<Producto>> EliminarProductoId(){
        System.out.println("Eliminar todos los productos");
        listaProductos.removeAll(listaProductos);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    //Eliminar Productos por ID
    @DeleteMapping(value="/V2/Productos/{id}")
    public ResponseEntity <String> EliminarProductoId(@PathVariable int id){
        ResponseEntity <String> respuesta= null;
        System.out.println("Eliminar productos por ID");
        try
        {
            Producto productoAModificar = listaProductos.get(id);
            System.out.println("Voy a eliminar el producto");
            listaProductos.remove(id);
            respuesta = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            respuesta= new ResponseEntity<>("No se ah encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

}
