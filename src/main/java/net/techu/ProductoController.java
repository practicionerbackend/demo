package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
class ProductoController {

    private ArrayList<String> listaProductos = null;

    public ProductoController() {
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");
    }

    //Obtener Productos
    @GetMapping(value="/Productos",produces="application/json")
    public ResponseEntity<List<String>> obtenerListado()
    {
        System.out.println("Estoy en obtener");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    //Obtener Productos por ID
    @GetMapping(value="/Productos/{id}",produces="application/json")
    public ResponseEntity <String> obtenerProductoId(@PathVariable int id)
    {
        String resultado =null;
        ResponseEntity <String> respuesta= null;
        try
        {
            resultado= listaProductos.get(id);
            respuesta= new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            resultado="No se ah encontrado el producto";
            respuesta= new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    //Icluir Producto PR3
    @PostMapping(value="/Productos")
    public ResponseEntity<String> addProducto() {
        System.out.println("Estoy en añadir");
        listaProductos.add("PR3");
        return new ResponseEntity<>("Producto Creado Correctamente", HttpStatus.CREATED);
    }

    //Incluir Producto por Postman
    @PostMapping(value="/Productos/{nom}")
    public ResponseEntity<String> addProductoconNombre(@PathVariable("nom") String nombre){
        listaProductos.add(nombre);
        return new ResponseEntity<>("Producto Creado Correctamente", HttpStatus.CREATED);
    }

    //Eliminar Productos por ID
    @DeleteMapping(value="/Productos/{id}")
    public ResponseEntity <String> EliminarProductoId(@PathVariable int id){
        String resultado =null;
        ResponseEntity <String> respuesta= null;
        try
        {
            resultado= listaProductos.get(id);
            respuesta= new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado="No se ah encontrado el producto";
            respuesta= new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

//    //Modificar Productos por ID
    @PutMapping (value="/Productos/{id}")
    public ResponseEntity <String> ModificarProductoId(@PathVariable int id){
        String resultado =null;
        ResponseEntity <String> respuesta= null;
        try
        {
            resultado= listaProductos.get(id);
            respuesta= new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado="No se ah encontrado el producto";
            respuesta= new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }
}
