package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

//@SpringBootApplication
public class ProductoV3Controller implements CommandLineRunner {

    @Autowired
    private ProductoRepositorio repository;

    public static void main(String[] args) {
        SpringApplication.run(ProductoV3Controller.class, args);
    }

    @Override
    public void run(String... args) throws Exception{
        System.out.println("Preparando MongoDB");
        repository.deleteAll();
        repository.insert(new ProductoMongo("JDPR1", "99"));
        repository.insert(new ProductoMongo("JDPR2", "88"));
        List<ProductoMongo> lista = repository.findAll();
        for (ProductoMongo p:lista) {
            System.out.println(p.toString());
        }
    }

}
