package net.techu;


import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductoV4Controller {


    @Autowired
    private ProductoRepositorio repositorio;

    //Obtener Productos
    @GetMapping(value = "/V4/Productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado() {
        System.out.println("Obtener todos los productos");
        List<ProductoMongo> lista = repositorio.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    //Incluir Producto por Postman
    @PostMapping(value = "/V4/Productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoMongo productoMongo) {
        ProductoMongo resultado = repositorio.insert(productoMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    @PutMapping(value = "/v4/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable String id, @RequestBody ProductoMongo productoMongo) {
        Optional<ProductoMongo> resultado = repositorio.findById(id);
        if (resultado.isPresent()) {
            ProductoMongo productoAModificar = resultado.get();
            productoAModificar.nombre = productoMongo.nombre;
            productoAModificar.color = productoMongo.color;
            productoAModificar.precio = productoMongo.precio;
        }
        ProductoMongo guardado = repositorio.save(resultado.get());
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    @PutMapping(value = "/V4/productos/{id}")
    public ResponseEntity<String> updateProducto2(@PathVariable String id, @RequestBody ProductoMongo productoMongo) {
        Optional<ProductoMongo> resultado = repositorio.findById(id);
        if (resultado.isPresent()) {
            ProductoMongo productoAModificar = resultado.get();
            productoAModificar.nombre = productoMongo.nombre;
            productoAModificar.color = productoMongo.color;
            productoAModificar.precio = productoMongo.precio;
            ProductoMongo guardado = repositorio.save(resultado.get());
            return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
        }
        return new ResponseEntity<String>("Producto no encontrado", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/v4/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id) {
        /* Posibilidad de buscar el producto, después eliminarlo si existe */
        repositorio.deleteById(id);
        return new ResponseEntity<String>("Producto borrado", HttpStatus.OK);
    }
}
