package net.techu.data;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductoRepositorio extends MongoRepository <ProductoMongo, String>  {
        public ProductoMongo findByNombre(String nombre);
    }
