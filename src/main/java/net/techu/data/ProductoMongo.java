package net.techu.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("productosJuanDavid")
public class ProductoMongo {

    @Id
    public String id;
    public String nombre;
    public String precio;
    public String color;

    public ProductoMongo() {
    }

    public ProductoMongo(String nombre, String precio) {
        this.nombre = nombre;
        this.precio = precio;
        this.color = color;
    }

    @Override
    public String toString(){
        return String.format("Producto [id=%s,nombre=%s,precio=%s,color=%s]",id,nombre,precio,color);
    }
}
